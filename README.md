# Sphinx Documentation CI/CD Components

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to enable building documentation pages with [Sphinx](https://www.sphinx-doc.org).

[[_TOC_]]

## Usage

### Components

#### build

Configures a job that executes `sphinx-build`.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/build@<VERSION>
```

##### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The pipeline stage to add jobs to |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `job_name` | `"sphinx"` | Name to give the build job |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image in which to build the documentation |
| `builder` | `"html"` | Sphinx [builder](https://www.sphinx-doc.org/en/master/usage/builders/) to use |
| `source_dir` | None | Path of directory containing the Sphinx configuration file (`conf.py`) |
| `requirements` | `""` | Extra packages to install (with pip) before building |
| `sphinx_options` | `""` | Extra options to pass to sphinx-build |
| `sphinx_outputdir` | `"sphinx"` | Sphinx build output directory |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch), or `"never!` (don't publish anything) |

##### Notes

-   Given the inputs, this component approximately executes the following
    command:

    ```shell
    python -m sphinx -b <builder> <sphinx_sourcedir> <sphinx_outputdir> <sphinx_options>
    ```

#### apidoc

Configures a job that automtically generates Sphinx sources using
[`sphinx-apidoc`](https://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html)
and then
executes [`sphinx-build`](https://www.sphinx-doc.org/en/master/man/sphinx-build.html).
via the [`sphinx/build` component](#build).

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/build@<VERSION>
```

##### Inputs

The inputs for this component are the same the
[inputs for the `sphinx/build` component](#build) with the following additions:

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `module_dir` | `"."` | Path of module(s) to document |
| `sphinx_apidoc_options` | See _Notes_ | Extra options to pass to `sphinx-apidoc` |
| `exclude` | See _Notes_ | `fnmatch`-style file and/or directory patterns to exclude from generation |

##### Notes

-   Given the inputs, this component approximately executes the following
    command:

    ```shell
    python -m sphinx.ext.apidoc -o <sphinx_sourcedir> <sphinx_apidoc_options> <module_dir> <exclude>
    ```

-   The default value for the `sphinx_apidoc_options` input is

    ```text
    --module-first --separate --full --ext-autodoc --ext-intersphinx --doc-project '$CI_PROJECT_NAME' --doc-version '$CI_COMMIT_REF_NAME'
    ```

#### pages

Configures a job that publishes the output of a Sphinx build with
[Gitlab Pages](https://git.ligo.org/help/user/project/pages/).

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/pages@<VERSION>
```

##### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The pipeline stage to add jobs to |
| `sphinx_outputdir` | `"sphinx"` | Sphinx build output directory |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch) |

##### Notes

-   This component will normally be easiest to configure via the `pages_when`
    input for the [`sphinx/build` component](#build), rather than as standalone.

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.
                                                                                                      All interactions related to this project should follow the                                            [LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).
                                                                                                      For more details on contributing to this project, see `CONTRIBUTING.md`.

## Releases

To create a new release of this component, go to
<https://git.ligo.org/computing/gitlab/components/sphinx/-/tags/new>
and create a new tag.
**You must use a semantic version for the tag name**, e.g. 1.2.3, without
any prefix or suffix.
Feel free to include a tag message, but this is not required.

The CI/CD pipeline triggered for the tag will then automatically create a new
[release](https://git.ligo.org/computing/gitlab/components/sphinx/-/releases)
which will be published to the
[CI/CD Catalog](https://git.ligo.org/explore/catalog/computing/gitlab/components/sphinx).
