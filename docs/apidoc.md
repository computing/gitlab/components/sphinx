# `sphinx/apidoc`

Configure a job to automatically configure a Sphinx documentation project using
[`sphinx-apidoc`](https://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html)
and then build it with
[`sphinx-build`](https://www.sphinx-doc.org/en/master/man/sphinx-build.html).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/apidoc@<VERSION>
    inputs:
      stage: build
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The pipeline stage to add jobs to |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `job_name` | `"sphinx"` | Name to give the build job |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image in which to build the documentation |
| `builder` | `"html"` | Sphinx [builder](https://www.sphinx-doc.org/en/master/usage/builders/) to use |
| `module_dir` | None | Path of module(s) to document |
| `apt_packages` | `""` | Packages to install with APT before building, this should mirror the [`build.apt_packages`](https://docs.readthedocs.com/platform/stable/config-file/v2.html#build-apt-packages) section of the `.readthedocs.yaml` file, if used by the project |
| `requirements` | `""` | Extra packages to install (with pip) before building |
| `sphinx_sourcedir` | `"_sphinx"` | Path of directory in which to create sphinx sources |
| `sphinx_apidoc_options` { .nowrap } | See [below](#sphinx_apidoc_options) { .nowrap } | Options to pass to sphinx-apidoc |
| `modify_script` | `[]` | Script commands to run _after_ apidoc has run, but _before_ the main Sphinx build is executed, to modify the sphinx sources before rendering |
| `sphinx_options` | `""` | Extra options to pass to sphinx-build |
| `sphinx_outputdir` | `"sphinx"` | Sphinx build output directory |
| `exclude` | `"setup.py tests examples"` { .nowrap } | `fnmatch`-style file and/or directory patterns to exclude from generation |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch), or `"never!` (don't publish anything) |

## Notes

### `sphinx_apidoc_options`

The default value for `sphinx_apidoc_options` is:

```text
--module-first --separate --full --ext-autodoc --ext-intersphinx --doc-project '$CI_PROJECT_NAME' --doc-version '$CI_COMMIT_REF_NAME'
```

## Customisation

### `modify_script`

The `modify_script` input can be used to run arbitrary shell commands _after_
`sphinx-apidoc` has run, but before the main `sphinx` run.
This can be useful to modify the auto-generated sources (changing the theme,
renaming files, etc).

Be careful to note the default value for the `sphinx_sourcedir` input,
or to explicitly set that option.

Extra packages required to support the modifications should be installed
via the `requirements` input.

## Examples

### Using `apidoc` to document a Python project { #example-apidoc }

!!! example "Use `apidoc` to document a Python project"

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/apidoc@$CI_COMMIT_SHA
        inputs:
          requirements: ".[docs]"
          modify_script:
            # move the top-level module doc into place as the index
            - mv _sphinx/myproject.rst _sphinx/index.rst
            # use the sphinx_rtd_theme
            - sed -i 's/alabaster/sphinx_rtd_theme/g' _sphinx/conf.py
    ```
