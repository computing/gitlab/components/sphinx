# `sphinx/build`

Configure a job to build a Sphinx documentation project.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/build@<VERSION>
    inputs:
      stage: build
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The pipeline stage to add jobs to |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `job_name` | `"sphinx"` | Name to give the build job |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image in which to build the documentation |
| `builder` | `"html"` | Sphinx [builder](https://www.sphinx-doc.org/en/master/usage/builders/) to use |
| `source_dir` | None | Path of directory containing the Sphinx configuration file (`conf.py`) |
| `requirements` | `""` | Extra packages to install (with pip) before building |
| `sphinx_options` | `""` | Extra options to pass to sphinx-build |
| `sphinx_outputdir` { .nowrap } | `"sphinx"` { .nowrap } | Sphinx build output directory |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch), or `"never!` (don't publish anything) |
