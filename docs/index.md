# Sphinx CI/CD components

The `computing/gitlab/components/sphinx` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to enable building documentation pages with [Sphinx](https://www.sphinx-doc.org).

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/sphinx/-/badges/release.svg)](https://git.ligo.org/explore/catalog/computing/gitlab/components/sphinx/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`sphinx/apidoc`](./apidoc.md "`sphinx/apidoc` component documentation")
- [`sphinx/build`](./build.md "`sphinx/build` component documentation")
- [`sphinx/pages`](./pages.md "`sphinx/pages` component documentation")
