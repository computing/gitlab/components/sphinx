# `sphinx/pages`

Configures a job that publishes the output of a Sphinx build with
[Gitlab Pages](https://git.ligo.org/help/user/project/pages/).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/sphinx/pages@<VERSION>
```

## Notes

### Automatically configurable from the build components { #auto }

Publication of Sphinx sites can be automated without manually including this
component by specifying the `pages_when` input for either the
[`sphinx/apidoc`](./apidoc.md) and [`sphinx/build`](./build.md) components.

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `"deploy"` | Pipeline stage to add jobs to (must be configured in `stages`) |
| `needs` | `["sphinx"]` { .nowrap } | The job(s) whose artefacts are to be published |
| `sphinx_outputdir` { .nowrap } | `"sphinx"` | Sphinx build output directory to publish |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch) |
