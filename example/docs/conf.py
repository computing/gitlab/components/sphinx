# Sphinx config

project = "example-project"
author = "Duncan Macleod"
copyright = "2024, Cardiff University"

extensions = [
    "sphinx.ext.autodoc",
]

html_theme = "furo"
